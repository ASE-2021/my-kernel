#if !defined(SEM_H)
#define SEM_H

#include "context.h"

#define SEM_MAGIC 0xCAFEBABE

typedef struct sem_s
{
    int cpt;
    ctx_t *first_ctx;
    int magic;
} sem_t;

int sem_init(sem_t *sem, int val);
void sem_up(sem_t *sem);
void sem_down(sem_t *sem);

#endif
