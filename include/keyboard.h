#if !defined(KEYBOARD_H)
#define KEYBOARD_H

#define NONE 0
#define SHIFT_UP 0x2A
#define SHIFT_DOWN 0xAA
#define NB_KEYS 256
#define QUEUE_SIZE 8

typedef enum boolean
{
    FALSE,
    TRUE
} boolean_t;

static boolean_t shift_pressed;

typedef struct key_s
{
    unsigned char bottom;
    unsigned char top;
} key_t;

typedef struct queue_s
{
    unsigned char first_free;
    unsigned char cpt;
    char array[QUEUE_SIZE];
} queue_t;

key_t KEY_MAPPING[NB_KEYS];
queue_t keyboard_queue;

void init_keymapping();

char keyboard_map(unsigned char code);

void init_queue();

void add_to_queue(unsigned char c);

char getc();

#endif
