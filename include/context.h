#if !defined(CONTEXT_H)
#define CONTEXT_H

#define STACK_SIZE 16384
#define CTX_MAX 16

#define SEM_MAGIC 0xCAFEBABE

typedef int(func_t)(int);
typedef void(funct_t)(void *);

typedef enum
{
    CTX_INIT,
    CTX_EXEC,
    CTX_WAIT,
    CTX_END
} ctx_status_t;

typedef struct ctx_s
{
    void *esp;
    void *ebp;
    char stack[STACK_SIZE];
    funct_t *f;
    void *args;
    ctx_status_t status;
    struct ctx_s *next_ctx;
    struct ctx_s *sem_next_ctx;
    char used;
} ctx_t;

static ctx_t ctx_heap[CTX_MAX];

ctx_t *malloc_ctx();

void free_ctx(ctx_t *ctx);

int init_ctx(ctx_t *, funct_t, void *);

void switch_to_ctx(ctx_t *);

void start_ctx();

ctx_t *create_ctx(funct_t, void *);

void yield();

void start_sched();

#endif // CONTEXT_H
