#if !defined(UTILS_H)
#define UTILS_H

#define NULL 0
#define EXIT_SUCCESS 0
#define assert(b) assert0((b), __FILE__, __FUNCTION__, __LINE__)

void clear_screen();            /* clear screen */
void putc(char aChar);          /* print a single char on screen */
void puts(const char *aString); /* print a string on the screen */
void puthex(int aNumber);       /* print an Hex number on screen */
void assert0(char b, const char *filename, const char *fct_name, const int line);

void irq_disable();
void irq_enable();

#endif // UTILS_H
