#include "idt.h"
#include "ioport.h"
#include "gdt.h"
#include "utils.h"
#include "context.h"
#include "sem.h"
#include "keyboard.h"

extern int cursor_x;
extern int cursor_y;
extern sem_t keyboard_sem;

int counter = 0;

ctx_t *ping_ctx = NULL;
ctx_t *pong_ctx = NULL;

sem_t sping;
sem_t spong;

void empty_irq(int_regs_t *r)
{
}

void timer_it(int_regs_t *r)
{
  yield();
}

void keyboard_it(int_regs_t *r)
{
  irq_disable();

  // puts("keyboard_it\n");
  if (_inb(0x64) == (0x1D))
  {
    _outb(0x64, 0x1C);
    char c = keyboard_map(_inb(0x60));
    add_to_queue(c);
  }

  irq_enable();
}

void counter_handler(void *args)
{
  while (1)
  {
    irq_disable();
    int tmp_cursor[2] = {cursor_x, cursor_y};
    cursor_x = 70;
    cursor_y = 0;

    puthex(counter++);

    cursor_x = tmp_cursor[0];
    cursor_y = tmp_cursor[1];
    irq_enable();
  }
}

void keyboard_handler(void *args)
{
  while (1)
  {
    irq_disable();
    char c = getc();
    if (c != NONE)
      putc(c);

    irq_enable();
  }
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int *mboot_info)
{
  /* clear the screen */
  clear_screen();
  puts("Early boot.\n");
  puts("\t-> Setting up the GDT... ");
  gdt_init_default();
  puts("done\n");

  puts("\t-> Setting up the IDT... ");
  setup_idt();
  puts("OK\n");

  puts("\n\n");

  idt_setup_handler(0, timer_it);
  idt_setup_handler(1, keyboard_it);

  __asm volatile("sti");

  /* minimal setup done ! */

  puts("Hello world\n\n");

  init_keymapping();
  init_queue();

  sem_init(&keyboard_sem, 0);

  ctx_t *counter_ctx = create_ctx(&counter_handler, NULL);
  ctx_t *keyboard_ctx = create_ctx(&keyboard_handler, NULL);

  yield();

  for (;;)
    ; /* nothing more to do... really nothing ! */
}
