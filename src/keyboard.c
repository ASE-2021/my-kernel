#include "keyboard.h"
#include "sem.h"
#include "utils.h"

sem_t keyboard_sem;

void init_keymapping()
{
    int i;
    for (int i = 0; i < NB_KEYS; i++)
        KEY_MAPPING[i] = (key_t){0, 0};

    shift_pressed = FALSE;

    // alphabetics letters and space
    KEY_MAPPING[0x1E] = (key_t){'a', 'A'};
    KEY_MAPPING[0x30] = (key_t){'b', 'B'};
    KEY_MAPPING[0x2E] = (key_t){'c', 'C'};
    KEY_MAPPING[0x20] = (key_t){'d', 'D'};
    KEY_MAPPING[0x12] = (key_t){'e', 'E'};
    KEY_MAPPING[0x21] = (key_t){'f', 'F'};
    KEY_MAPPING[0x22] = (key_t){'g', 'G'};
    KEY_MAPPING[0x23] = (key_t){'h', 'H'};
    KEY_MAPPING[0x17] = (key_t){'i', 'I'};
    KEY_MAPPING[0x24] = (key_t){'j', 'J'};
    KEY_MAPPING[0x25] = (key_t){'k', 'K'};
    KEY_MAPPING[0x26] = (key_t){'l', 'L'};
    KEY_MAPPING[0x32] = (key_t){'m', 'M'};
    KEY_MAPPING[0x31] = (key_t){'n', 'N'};
    KEY_MAPPING[0x18] = (key_t){'o', 'O'};
    KEY_MAPPING[0x19] = (key_t){'p', 'P'};
    KEY_MAPPING[0x10] = (key_t){'q', 'Q'};
    KEY_MAPPING[0x13] = (key_t){'r', 'R'};
    KEY_MAPPING[0x1F] = (key_t){'s', 'S'};
    KEY_MAPPING[0x14] = (key_t){'t', 'T'};
    KEY_MAPPING[0x16] = (key_t){'u', 'U'};
    KEY_MAPPING[0x2F] = (key_t){'v', 'V'};
    KEY_MAPPING[0x11] = (key_t){'w', 'W'};
    KEY_MAPPING[0x2D] = (key_t){'x', 'X'};
    KEY_MAPPING[0x15] = (key_t){'y', 'Y'};
    KEY_MAPPING[0x2C] = (key_t){'z', 'Z'};
    KEY_MAPPING[0x39] = (key_t){' ', ' '};

    // Numeric row
    KEY_MAPPING[0x02] = (key_t){'1', '!'};
    KEY_MAPPING[0x03] = (key_t){'2', '@'};
    KEY_MAPPING[0x04] = (key_t){'3', '#'};
    KEY_MAPPING[0x05] = (key_t){'4', '$'};
    KEY_MAPPING[0x06] = (key_t){'5', '%'};
    KEY_MAPPING[0x07] = (key_t){'6', '^'};
    KEY_MAPPING[0x08] = (key_t){'7', '&'};
    KEY_MAPPING[0x09] = (key_t){'8', '*'};
    KEY_MAPPING[0x0A] = (key_t){'9', '('};
    KEY_MAPPING[0x0B] = (key_t){'0', ')'};
    KEY_MAPPING[0x0C] = (key_t){'-', '_'};
    KEY_MAPPING[0x28] = (key_t){'\'', '"'};

    // Addons
    KEY_MAPPING[0x1C] = (key_t){'\n', '\n'};
    KEY_MAPPING[0x0F] = (key_t){'\t', '\t'};
    KEY_MAPPING[0x0E] = (key_t){'\b', '\b'}; // del

    // Punctuation signs
    KEY_MAPPING[0x0D] = (key_t){'=', '+'};
    KEY_MAPPING[0x27] = (key_t){';', ':'};
    KEY_MAPPING[0x33] = (key_t){',', '<'};
    KEY_MAPPING[0x34] = (key_t){'.', '>'};
    KEY_MAPPING[0x35] = (key_t){'/', '?'};
    KEY_MAPPING[0x1A] = (key_t){'[', '{'};
    KEY_MAPPING[0x1B] = (key_t){']', '}'};
    KEY_MAPPING[0x2B] = (key_t){0, '|'};
    KEY_MAPPING[0x29] = (key_t){'`', 0};
}

char keyboard_map(unsigned char code)
{
    if (code == SHIFT_UP)
        shift_pressed = TRUE;
    else if (code == SHIFT_DOWN)
        shift_pressed = FALSE;

    if (shift_pressed)
        return KEY_MAPPING[code].top;
    return KEY_MAPPING[code].bottom;
}

void init_queue()
{
    keyboard_queue.cpt = 0;
    keyboard_queue.first_free = 0;
}

void add_to_queue(unsigned char c)
{
    irq_disable();

    if (c != NONE && keyboard_queue.cpt < QUEUE_SIZE)
    {
        keyboard_queue.array[keyboard_queue.first_free] = c;
        keyboard_queue.first_free = (keyboard_queue.first_free + 1) % QUEUE_SIZE;
        keyboard_queue.cpt++;
        sem_up(&keyboard_sem);
    }

    irq_enable();
}

char getc()
{
    irq_disable();

    char c = NONE;
    if (keyboard_queue.cpt)
    {
        sem_down(&keyboard_sem);
        int idx = (keyboard_queue.first_free + QUEUE_SIZE - keyboard_queue.cpt) % QUEUE_SIZE;
        c = keyboard_queue.array[idx];
        keyboard_queue.cpt--;
    }

    irq_enable();
    return c;
}