#include "sem.h"
#include "context.h"
#include "utils.h"

extern ctx_t *current_ctx;

int sem_init(sem_t *sem, int val)
{
    assert(sem->magic != SEM_MAGIC);

    sem->magic = SEM_MAGIC;
    sem->cpt = val;
    sem->first_ctx = NULL;

    return 0;
}

void sem_up(sem_t *sem)
{
    assert(sem->magic == SEM_MAGIC);

    irq_disable();
    if ((++sem->cpt) <= 0)
    {
        sem->first_ctx->status = CTX_EXEC;
        sem->first_ctx = sem->first_ctx->sem_next_ctx;
    }
    irq_enable();
}

void sem_down(sem_t *sem)
{
    assert(sem->magic == SEM_MAGIC);

    irq_disable();
    if ((--sem->cpt) < 0)
    {
        current_ctx->status = CTX_WAIT;
        current_ctx->sem_next_ctx = sem->first_ctx;
        sem->first_ctx = current_ctx;
        irq_enable();
        yield();
    }
    irq_enable();
}